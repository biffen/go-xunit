package xunit

import (
	"context"
	"io"
	"strconv"
	"time"

	"gitlab.com/biffen/go-xunit/internal"
	"gitlab.com/biffen/go-xunit/internal/xml"
	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

var _ Statser = (*TestSuites)(nil)

type TestSuites struct {
	ID         string
	Name       string
	Time       *time.Duration
	TestSuites []TestSuite
}

func testSuitesFromXML(
	x xunitxml.TestSuites,
	options internal.Options,
) *TestSuites {
	s := &TestSuites{
		ID:         x.ID,
		Name:       x.Name,
		Time:       x.Time.ToDuration(),
		TestSuites: make([]TestSuite, 0, len(x.TestSuites)),
	}

	for i, xs := range x.TestSuites {
		ss := testSuiteFromXML(xs, options)
		if options.AutoID && ss.ID == "" {
			ss.ID = strconv.Itoa(i)
		}
		s.TestSuites = append(s.TestSuites, ss)
	}

	return s
}

func (s *TestSuites) OK() bool {
	stats := s.Stats()
	return stats.Failures+stats.Errors == 0
}

func (suites *TestSuites) Print(
	ctx context.Context,
	w io.Writer,
	options ...Option,
) error {
	return printXML[xunitxml.TestSuites](ctx, suites, w, options...)
}

func (s *TestSuites) Stats() (stats Stats) {
	for _, ss := range s.TestSuites {
		stats = stats.Merge(ss.Stats())
	}

	return
}

func (s *TestSuites) toXML(options internal.Options) xunitxml.TestSuites {
	x := xunitxml.TestSuites{
		ID:         s.ID,
		Name:       s.Name,
		Time:       internal.NewTime(s.Time, options),
		Comment:    xml.CleanUpComment(&options.Comment),
		TestSuites: make([]xunitxml.TestSuite, 0, len(s.TestSuites)),
	}

	options.Comment = ""

	for i, ss := range s.TestSuites {
		xss := ss.toXML(options)
		if options.AutoID && xss.ID == "" {
			xss.ID = strconv.Itoa(i)
		}
		x.TestSuites = append(x.TestSuites, xss)

		stats := ss.Stats()
		x.Tests += stats.Tests
		x.Disabled += stats.Disabled
		x.Failures += stats.Failures
		x.Errors += stats.Errors
	}

	return x
}
