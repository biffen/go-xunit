package main

import (
	"context"
	_ "embed"
	"fmt"
	"io"
	"math/big"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/biffen/go-applause"
	"gitlab.com/biffen/go-xunit"
)

//go:embed usage.txt
var usage string

func humanNumber(u uint64) string {
	var b big.Int
	b.SetUint64(u)
	return humanize.BigComma(&b)
}

func humanTime(t *time.Time) string {
	if t == nil {
		return "<nil>"
	}

	return fmt.Sprintf("%s (%s)", t.Local(), humanize.Time(*t))
}

func inner(ctx context.Context) (exit int, err error) {
	var parser *applause.Parser
	if parser, err = applause.NewParser(); err != nil {
		return
	}

	var help bool
	if err = parser.Add("h|help", &help); err != nil {
		return
	}

	var operands []string
	if operands, err = parser.Parse(ctx, os.Args[1:]); err != nil {
		return
	}

	if help {
		fmt.Print(usage)
		return
	}

	if len(operands) == 0 {
		operands = []string{"-"}
	}

	for _, operand := range operands {
		var (
			file string
			r    io.Reader
		)

		if operand == "-" {
			file = "<STDIN>"
			r = os.Stdin
		} else {
			f, err := os.Open(operand)
			if err != nil {
				return 0, err
			}
			file = operand
			r = f
		}

		var testsuites *xunit.TestSuites
		if testsuites, err = xunit.Parse(ctx, r); err != nil {
			return
		}

		stats := testsuites.Stats()

		fmt.Printf(`%s:
  ID:       %s
  Name:     %s
  Time:     %s
  Suites:   %s
  Tests:    %s
  Disabled: %s
  Errors:   %s
  Failures: %s
  Skipped:  %s
`,
			file,
			quote(testsuites.ID),
			quote(testsuites.Name),
			testsuites.Time,
			humanNumber(uint64(len(testsuites.TestSuites))),
			humanNumber(stats.Tests),
			humanNumber(stats.Disabled),
			humanNumber(stats.Failures),
			humanNumber(stats.Errors),
			humanNumber(stats.Skipped),
		)

		if !testsuites.OK() {
			exit = 3
		}

		for i, suite := range testsuites.TestSuites {
			stats := suite.Stats()

			fmt.Printf(`  %d:
    ID:        %s
    Name:      %s
    Time:      %s
    Timestamp: %s
    Tests:     %s
    Disabled:  %s
    Errors:    %s
    Failures:  %s
    Skipped:   %s
`,
				i,
				quote(suite.ID),
				quote(suite.Name),
				suite.Time,
				humanTime(suite.Timestamp),
				humanNumber(stats.Tests),
				humanNumber(stats.Disabled),
				humanNumber(stats.Failures),
				humanNumber(stats.Errors),
				humanNumber(stats.Skipped),
			)
		}
	}

	return
}

func main() {
	ctx, stop := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGTERM,
	)
	defer stop()

	exit, err := inner(ctx)
	if err != nil {
		panic(err)
	}

	os.Exit(exit)
}

func quote(str string) string {
	if str == "" {
		return "<nil>"
	}

	return strconv.Quote(str)
}
