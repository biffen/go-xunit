package xunit

import (
	"context"
	"encoding/xml"
	"fmt"
	"io"

	"gitlab.com/biffen/go-xunit/internal"
)

func printXML[T any](
	_ context.Context,
	t toXMLer[T],
	w io.Writer,
	options ...Option,
) error {
	o := initOptions(options)

	if _, err := fmt.Fprint(w, xml.Header); err != nil {
		return fmt.Errorf("failed to print xUnit XML: %w", err)
	}

	enc := xml.NewEncoder(w)
	enc.Indent("", "  ")

	x := t.toXML(o)

	err := enc.Encode(x)

	if err == nil {
		_, err = fmt.Fprint(w, "\n")
	}

	if err != nil {
		return fmt.Errorf("failed to encode xUnit XML: %w", err)
	}

	return nil
}

type toXMLer[T any] interface {
	toXML(options internal.Options) T
}
