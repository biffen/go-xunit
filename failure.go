package xunit

import (
	"gitlab.com/biffen/go-xunit/internal"
	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

type Failure struct {
	Message     string
	Type        string
	Description string
}

func failureFromXML(x *xunitxml.Failure, options internal.Options) *Failure {
	if x == nil {
		return nil
	}

	return &Failure{
		Message:     x.Message,
		Type:        x.Type,
		Description: x.Description.String(),
	}
}

func (f *Failure) toXML(options internal.Options) *xunitxml.Failure {
	if f == nil {
		return nil
	}

	return &xunitxml.Failure{
		Message:     f.Message,
		Type:        f.Type,
		Description: internal.NewText(f.Description),
	}
}
