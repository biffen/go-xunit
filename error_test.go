package xunit_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/go-xunit"
)

var _ error = (*errTest)(nil)

func TestErrorFromError(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Input    error
		Expected *xunit.Error
	}{
		{},

		{
			Input: fmt.Errorf("test"),
			Expected: &xunit.Error{
				Message:     "test",
				Type:        "errors.errorString",
				Description: "test",
			},
		},

		{
			Input: errTest("test"),
			Expected: &xunit.Error{
				Message:     "test",
				Type:        "gitlab.com/biffen/go-xunit_test.errTest",
				Description: "test",
			},
		},
	} {
		c := c

		t.Run(fmt.Sprintf("%T %v", c.Input, c.Input), func(t *testing.T) {
			t.Parallel()

			actual := xunit.ErrorFromError(c.Input)

			assert.Equal(t, c.Expected, actual)
		})
	}
}

type errTest string

func (err errTest) Error() string {
	return string(err)
}
