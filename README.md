# go-xunit

-   Apache Ant: https://github.com/windyroad/JUnit-Schema
-   IBM: https://www.ibm.com/docs/en/developer-for-zos/14.1?topic=formats-junit-xml-format
-   LLG: https://llg.cubic.org/docs/junit/
-   Surefire: https://maven.apache.org/surefire/maven-surefire-plugin/xsd/surefire-test-report-3.0.xsd

|                         | Apache Ant        | IBM       | LLG              | Surefire |
| ----------------------- | ----------------- | --------- | ---------------- | -------- |
| Root `testsuite`        | ?                 | ?         | Yes              | Yes      |
| Root `testsuites`       | Yes               | Yes       | Yes              | N/A      |
| `testcase#classname`    | Required          | —         | Required         | Optional |
| `testcase#group`        | —                 | —         | Optional         | Optional |
| `testcase#id`           | —                 | Mentioned | —                | —        |
| `testcase#name`         | Required          | Mentioned | Required         | Required |
| `testcase#time`         | Required          | Mentioned | Optional         | Required |
| `testcase.error`        | Optional          | —         | Optional         | Optional |
| `testcase.failure`      | Optional          | Optional  | Optional         | Optional |
| `testcase.flakyError`   | —                 | —         | Optional         | Optional |
| `testcase.flakyFailure` | —                 | —         | Optional         | Optional |
| `testcase.rerunError`   | —                 | —         | Optional         | Optional |
| `testcase.rerunFailure` | —                 | —         | Optional         | Optional |
| `testcase.skipped`      | Optional          | —         | Optional         | Optional |
| `testcase.system-err`   | Optional          | —         | Optional         | Optional |
| `testcase.system-out`   | Optional          | —         | Optional         | Optional |
| `testsuite#errors`      | Required          | —         | Optional         | Required |
| `testsuite#failures`    | Required          | Mentioned | Optional         | Required |
| `testsuite#group`       | —                 | —         | Optional         | Optional |
| `testsuite#hostname`    | Required          | —         | Optional         | —        |
| `testsuite#id`          | Required, integer | Mentioned | Optional, string | —        |
| `testsuite#name`        | Required          | Mentioned | Required         | Required |
| `testsuite#package`     | Required          | —         | Optional         | —        |
| `testsuite#skipped`     | Optional          | —         | Optional         | Required |
| `testsuite#tests`       | Required          | Mentioned | Required         | Required |
| `testsuite#time`        | Required          | Mentioned | Optional         | Optional |
| `testsuite#timestamp`   | Required          | —         | Optional         | —        |
| `testsuite.properties`  | Mentioned         | —         | Optional         | Optional |
| `testsuite.system-err`  | —                 | —         | Optional         | —        |
| `testsuite.system-out`  | —                 | —         | Optional         | —        |
| `testsuites#disabled`   | —                 | —         | Optional         | N/A      |
| `testsuites#errors`     | —                 | —         | Optional         | N/A      |
| `testsuites#failures`   | —                 | Mentioned | Optional         | N/A      |
| `testsuites#id`         | —                 | Mentioned | —                | —        |
| `testsuites#name`       | —                 | Mentioned | Optional         | N/A      |
| `testsuites#tests`      | —                 | Mentioned | Optional         | N/A      |
| `testsuites#time`       | —                 | Mentioned | Optional         | N/A      |

| Legend    | Element or attribute…                                          |
| --------- | -------------------------------------------------------------- |
| Required  | … _must_ appear                                                |
| Optional  | … _may_ appear                                                 |
| Mentioned | … is documented, but whether it is required or optional is not |
| —         | … is not mentioned                                             |
| N/A       | … is not applicable                                            |
