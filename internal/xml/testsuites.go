package xml

import (
	"encoding/xml"

	"gitlab.com/biffen/go-xunit/internal"
)

type TestSuites struct {
	XMLName xml.Name `xml:"testsuites"`

	ID string `xml:"id,attr,omitempty"`

	// Name is the collective name of the suites.
	Name string `xml:"name,attr"`

	// Tests is the total number of tests from all testsuites. Some software may
	// expect to only see the number of successful tests from all testsuites
	// though.
	Tests uint64 `xml:"tests,attr"`

	// Disabled is the total number of disabled tests from all testsuites.
	Disabled uint64 `xml:"disabled,attr,omitempty"`

	// Failures is the total number of failed tests from all testsuites.
	Failures uint64 `xml:"failures,attr"`

	// Errors is the total number of tests with error result from all
	// testsuites.
	Errors uint64 `xml:"errors,attr"`

	// Time in seconds to execute all test suites.
	Time *internal.Time `xml:"time,attr,omitempty"`

	Comment string `xml:",comment"`

	TestSuites []TestSuite `xml:"testsuite"`
}
