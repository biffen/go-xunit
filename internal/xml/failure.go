package xml

import (
	"encoding/xml"

	"gitlab.com/biffen/go-xunit/internal"
)

// Failure indicates that the test failed. A failure is a condition which the
// code has explicitly failed by using the mechanisms for that purpose. For
// example via an assertEquals. Contains as a text node relevant data for the
// failure, e.g., a stack trace.
//
// Optional.
type Failure struct {
	XMLName xml.Name `xml:"failure"`

	// Message is the message specified in the assert.
	Message string `xml:"message,attr,omitempty"`

	// The type of the assert.
	Type string `xml:"type,attr,omitempty"`

	Description internal.Text `xml:",chardata"`
}
