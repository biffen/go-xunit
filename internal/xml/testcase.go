package xml

import (
	"encoding/xml"

	"gitlab.com/biffen/go-xunit/internal"
)

type Skipped struct {
	XMLName xml.Name `xml:"skipped"`

	// Message/description string why the test case was skipped.
	//
	// Optional.
	Message string `xml:"message,attr,omitempty"`
}

// TestCase can appear multiple times, see /testsuites/testsuite@tests.
type TestCase struct {
	XMLName xml.Name `xml:"testcase"`

	ID string `xml:"id,attr,omitempty"`

	// File is a non-standard attribute used by, at least, GitLab.
	File string `xml:"file,attr,omitempty"`

	// Name of the test method, required.
	Name string `xml:"name,attr"`

	// Assertions is number of assertions in the test case.
	//
	// Optional.
	Assertions uint64 `xml:"assertions,attr,omitempty"`

	// Classname is the full class name for the class the test method is in.
	//
	// Required.
	Classname string `xml:"classname,attr,omitempty"`

	Status string `xml:"status,attr,omitempty"`

	// Time taken (in seconds) to execute the test.
	//
	// Optional.
	Time *internal.Time `xml:"time,attr,omitempty"`

	// Skipped can appear 0 or once.
	//
	// If the test was not executed or failed, you can specify one of the
	// skipped, error or failure elements.
	Skipped *Skipped

	Error *Error

	Failure *Failure

	// Data that was written to standard out while the test was executed.
	//
	// Optional.
	SystemOut internal.Text `xml:"system-out,omitempty"`

	// Data that was written to standard error while the test was executed.
	//
	// Optional.
	SystemErr internal.Text `xml:"system-err,omitempty"`
}
