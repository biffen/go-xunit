package xml

import (
	"encoding/xml"

	"gitlab.com/biffen/go-xunit/internal"
)

// Error indicates that the test errored. An errored test had an unanticipated
// problem. For example an unchecked throwable (exception), crash or a problem
// with the implementation of the test. Contains as a text node relevant data
// for the error, for example a stack trace.
//
// Optional.
type Error struct {
	XMLName xml.Name `xml:"error"`

	// Message is the error message. e.g., if a java exception is thrown, the
	// return value of getMessage()
	Message string `xml:"message,attr,omitempty"`

	// The type of error that occurred. e.g., if a java exception is thrown the
	// full class name of the exception.
	Type string `xml:"type,attr,omitempty"`

	Description internal.Text `xml:",chardata"`
}
