package xml

import "strings"

func CleanUpComment(comment *string) string {
	if comment == nil {
		return ""
	}

	in := *comment
	out := in
	for {
		out = strings.ReplaceAll(in, "--", "- -")
		if out == in {
			break
		}
		in = out
	}

	*comment = ""

	return out
}
