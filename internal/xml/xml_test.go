package xml_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/go-xunit/internal/xml"
)

func TestCleanUpComment(t *testing.T) {
	t.Parallel()

	comment := "foo --- bar"
	clean := xml.CleanUpComment(&comment)
	assert.Empty(t, comment)
	assert.Equal(t, "foo - - - bar", clean)
}
