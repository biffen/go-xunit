package xml

import (
	"encoding/xml"

	"gitlab.com/biffen/go-xunit/internal"
)

type Properties struct {
	XMLName  xml.Name   `xml:"properties"`
	Property []Property `xml:"property"`
}

// Property can appear multiple times. The name and value attributres
// are required.
type Property struct {
	Name  string `xml:"name,attr"`
	Value string `xml:"value,attr"`
}

// TestSuite can appear multiple times, if contained in a testsuites element. It
// can also be the root element.
type TestSuite struct {
	XMLName xml.Name `xml:"testsuite"`

	ID string `xml:"id,attr,omitempty"`

	// Name is the full (class) name of the test for non-aggregated testsuite
	// documents.
	Name string `xml:"name,attr"`

	// Tests is the total number of tests in the suite, required.
	Tests uint64 `xml:"tests,attr"`

	// Disabled is the total number of disabled tests in the suite.
	//
	// Optional.
	Disabled uint64 `xml:"disabled,attr,omitempty"`

	// Failures is the total number of tests in the suite that failed. A failure
	// is a test which the code has explicitly failed
	Failures uint64 `xml:"failures,attr"`

	// Errors is the total number of tests in the suite that errored. An errored
	// test is one that had an unanticipated problem,
	Errors uint64 `xml:"errors,attr"`

	// Skipped is the total number of skipped tests.
	//
	// Optional.
	Skipped uint64 `xml:"skipped,attr,omitempty"`

	// For is the example an unchecked throwable; or a problem with the
	// implementation of the test.
	//
	// Optional.
	For string `xml:"for,attr,omitempty"`

	// By using the mechanisms for that purpose. e.g., via an assertEquals.
	//
	// Optional.
	By string `xml:"by,attr,omitempty"`

	// Hostname is the host on which the tests were executed. 'localhost' should
	// be used if the hostname cannot be determined.
	//
	// Optional.
	Hostname string `xml:"hostname,attr,omitempty"`

	// Package is derived from testsuite/@name in the non-aggregated documents.
	//
	// Optional.
	Package string `xml:"package,attr,omitempty"`

	// Time taken (in seconds) to execute the tests in the suite.
	//
	// Optional.
	Time *internal.Time `xml:"time,attr,omitempty"`

	// Timestamp when the test was executed in ISO 8601 format
	// (2014-01-21T16:17:18). Timezone may not be specified.
	//
	// Optional.
	Timestamp *internal.Timestamp `xml:"timestamp,attr,omitempty"`

	Comment string `xml:",comment"`

	// Properties (e.g., environment settings) set during test execution. The
	// properties element can appear 0 or once.
	Properties *Properties `xml:",omitempty"`

	TestCases []TestCase `xml:"testcase"`

	// Data that was written to standard out while the test suite was executed.
	//
	// Optional.
	SystemOut internal.Text `xml:"system-out,omitempty"`

	// Data that was written to standard error while the test suite was
	// executed.
	//
	// Optional.
	SystemErr internal.Text `xml:"system-err,omitempty"`
}
