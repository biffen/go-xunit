package internal

import (
	"encoding"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

var (
	_ encoding.TextMarshaler   = (*Time)(nil)
	_ encoding.TextUnmarshaler = (*Time)(nil)
)

type Time struct {
	time.Duration
	options Options
}

func NewTime(d *time.Duration, options Options) *Time {
	if d == nil || *d <= 0 {
		return nil
	}

	return &Time{
		Duration: *d,
		options:  options,
	}
}

func (t Time) MarshalText() (text []byte, err error) {
	if t.options.TimeGrouping {
		return []byte(
			humanize.CommafWithDigits(t.Seconds(), int(t.options.TimeDecimals)),
		), nil
	}

	return []byte(
		humanize.FtoaWithDigits(t.Seconds(), int(t.options.TimeDecimals)),
	), nil
}

func (t *Time) ToDuration() *time.Duration {
	if t == nil {
		return nil
	}

	tmp := t.Duration
	return &tmp
}

func (t *Time) UnmarshalText(text []byte) error {
	tmp, err := strconv.ParseFloat(
		strings.ReplaceAll(string(text), ",", ""),
		64,
	)
	if err != nil {
		return err
	}

	*t = Time{
		Duration: time.Duration(tmp * float64(time.Second)),
	}

	return nil
}
