package internal

type Options struct {
	AutoID       bool
	Comment      string
	TimeDecimals uint8
	TimeGrouping bool
}

func NewOptions() Options {
	return Options{
		AutoID:       false,
		TimeDecimals: 3,
		TimeGrouping: true,
	}
}
