package internal_test

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/go-xunit/internal"
)

func TestDuration_MarshalXML(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		S
		string
		error
	}{
		{S{}, `<S></S>`, nil},
		{S{Duration: new(internal.Time)}, `<S duration="0"></S>`, nil},
		{S{Duration: newTime(100 * time.Millisecond)}, `<S duration="0.1"></S>`, nil},
		{S{Duration: newTime((42_000 * time.Second) + (17 * time.Millisecond))}, `<S duration="42,000.017"></S>`, nil},
	} {
		c := c

		t.Run(fmt.Sprintf("%#v", c.S), func(t *testing.T) {
			t.Parallel()

			x, err := xml.Marshal(c.S)

			if c.error != nil {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, c.string, string(x))
		})
	}
}

func TestDuration_UnmarshalXML(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		string
		S
		error
	}{
		{`<S></S>`, S{}, nil},
		{`<S duration="1"></S>`, S{&internal.Time{Duration: time.Second}}, nil},
		{`<S duration="0.1"></S>`, S{&internal.Time{Duration: 100 * time.Millisecond}}, nil},
		{`<S duration="42,000.017"></S>`, S{&internal.Time{Duration: (42_000 * time.Second) + (17 * time.Millisecond)}}, nil},
		{`<S duration="foobar"></S>`, S{}, strconv.ErrSyntax},
	} {
		c := c

		t.Run(fmt.Sprintf("%#v", c.S), func(t *testing.T) {
			t.Parallel()

			var s S

			err := xml.Unmarshal([]byte(c.string), &s)

			if c.error != nil {
				assert.ErrorIs(t, err, c.error)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, c.S, s)
		})
	}
}

func newTime(d time.Duration) *internal.Time {
	return internal.NewTime(&d, internal.NewOptions())
}

type S struct {
	Duration *internal.Time `xml:"duration,attr,omitempty"`
}
