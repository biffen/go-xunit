package internal

import (
	"encoding"
	"time"

	errör "gitlab.com/biffen/error"
)

const (
	timestampFormat = "2006-01-02T15:04:05.999999999"
)

var (
	_ encoding.TextMarshaler   = (*Timestamp)(nil)
	_ encoding.TextUnmarshaler = (*Timestamp)(nil)
)

type Timestamp struct {
	time.Time
	options Options
}

func NewTimestamp(t *time.Time, options Options) *Timestamp {
	if t == nil {
		return nil
	}

	return &Timestamp{
		Time:    *t,
		options: options,
	}
}

func (t Timestamp) MarshalText() (text []byte, err error) {
	return []byte(t.UTC().Format(timestampFormat)), nil
}

func (t *Timestamp) ToTime(options Options) *time.Time {
	if t == nil {
		return nil
	}

	tmp := t.Time
	return &tmp
}

func (t *Timestamp) UnmarshalText(text []byte) error {
	var errors error

	for _, format := range [...]string{
		time.RFC3339Nano,
		timestampFormat,
	} {
		tmp, err := time.Parse(format, string(text))
		if err != nil {
			errors = errör.Composite(errors, err)
			continue
		}

		t.Time = tmp

		return nil
	}

	return errors
}
