package internal

import (
	"bytes"
	"encoding"
	"fmt"
)

var (
	_ encoding.TextMarshaler   = (*Text)(nil)
	_ encoding.TextUnmarshaler = (*Text)(nil)
	_ fmt.Stringer             = (*Text)(nil)
)

type Text []byte

func NewText(str string) Text {
	if str == "" {
		return nil
	}

	return Text(str)
}

func (t Text) MarshalText() (text []byte, err error) {
	return t, nil
}

func (t Text) String() string {
	return string(t)
}

func (t *Text) UnmarshalText(text []byte) error {
	if text != nil {
		*t = bytes.TrimSpace(text)
	}

	return nil
}
