package internal_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-xunit/internal"
)

func TestTimestamp_MarshalText(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Timestamp internal.Timestamp
		Expected  string
	}{
		{
			internal.Timestamp{Time: errör.Must(time.Parse(time.Layout, time.Layout))},
			"2006-01-02T22:04:05",
		},
	} {
		c := c

		t.Run(fmt.Sprintf("%v", c), func(t *testing.T) {
			t.Parallel()

			actual, err := c.Timestamp.MarshalText()
			assert.NoError(t, err)
			assert.Equal(t, c.Expected, string(actual))
		})
	}
}

func TestTimestamp_UnmarshalText(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Input    string
		Expected internal.Timestamp
		Error    error
	}{
		{
			Input:    "2006-01-02T22:04:05",
			Expected: internal.Timestamp{Time: errör.Must(time.Parse(time.Layout, time.Layout))},
		},
	} {
		c := c

		t.Run(fmt.Sprintf("%v", c), func(t *testing.T) {
			t.Parallel()

			var actual internal.Timestamp
			err := actual.UnmarshalText([]byte(c.Input))
			assert.NoError(t, err)
			assert.Equal(t, c.Expected.UTC(), actual.UTC())
		})
	}
}
