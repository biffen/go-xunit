package xunit

import (
	"context"
	"io"
	"strconv"
	"time"

	"gitlab.com/biffen/go-xunit/internal"
	"gitlab.com/biffen/go-xunit/internal/xml"
	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

var _ Statser = (*TestSuite)(nil)

type Property struct {
	Name  string
	Value string
}

type TestSuite struct {
	ID         string
	Name       string
	For        string
	By         string
	Hostname   string
	Package    string
	Disabled   uint64
	Time       *time.Duration
	Timestamp  *time.Time
	Properties []Property
	TestCases  []TestCase
	SystemOut  []byte `xml:"system-out,omitempty"`
	SystemErr  []byte `xml:"system-err,omitempty"`
}

func testSuiteFromXML(
	x xunitxml.TestSuite,
	options internal.Options,
) TestSuite {
	s := TestSuite{
		ID:        x.ID,
		Name:      x.Name,
		For:       x.For,
		By:        x.By,
		Hostname:  x.Hostname,
		Package:   x.Package,
		Disabled:  x.Disabled,
		Time:      x.Time.ToDuration(),
		Timestamp: x.Timestamp.ToTime(options),
		TestCases: make([]TestCase, 0, len(x.TestCases)),
		SystemOut: x.SystemOut,
		SystemErr: x.SystemErr,
	}

	if x.Properties != nil {
		s.Properties = make([]Property, len(x.Properties.Property))
		for i := range s.Properties {
			s.Properties[i] = Property{
				Name:  x.Properties.Property[i].Name,
				Value: x.Properties.Property[i].Value,
			}
		}
	}

	for i, c := range x.TestCases {
		c := testCaseFromXML(c, options)
		if options.AutoID && c.ID == "" {
			c.ID = strconv.Itoa(i)
		}
		s.TestCases = append(s.TestCases, c)
	}

	return s
}

func (suite *TestSuite) Print(
	ctx context.Context,
	w io.Writer,
	options ...Option,
) error {
	return printXML[xunitxml.TestSuite](ctx, suite, w, options...)
}

func (s *TestSuite) Stats() (stats Stats) {
	stats.Disabled = s.Disabled

	for _, c := range s.TestCases {
		stats = stats.Merge(c.Stats())
	}

	return
}

func (s *TestSuite) toXML(options internal.Options) xunitxml.TestSuite {
	x := xunitxml.TestSuite{
		ID:        s.ID,
		Name:      s.Name,
		For:       s.For,
		By:        s.By,
		Hostname:  s.Hostname,
		Package:   s.Package,
		Disabled:  s.Disabled,
		Time:      internal.NewTime(s.Time, options),
		Timestamp: internal.NewTimestamp(s.Timestamp, options),
		Comment:   xml.CleanUpComment(&options.Comment),
		TestCases: make([]xunitxml.TestCase, 0, len(s.TestCases)),
		SystemOut: s.SystemOut,
		SystemErr: s.SystemErr,
	}

	if len(s.Properties) > 0 {
		x.Properties = &xunitxml.Properties{
			Property: make([]xunitxml.Property, len(s.Properties)),
		}
		for i, p := range s.Properties {
			x.Properties.Property[i].Name, x.Properties.Property[i].Value = p.Name, p.Value
		}
	}

	for i, c := range s.TestCases {
		xc := c.toXML(options)
		if options.AutoID && xc.ID == "" {
			xc.ID = strconv.Itoa(i)
		}
		x.TestCases = append(x.TestCases, xc)

		stats := c.Stats()
		x.Tests += stats.Tests
		x.Disabled += stats.Disabled
		x.Failures += stats.Failures
		x.Errors += stats.Errors
		x.Skipped += stats.Skipped
	}

	return x
}
