package xunit_test

import (
	"context"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-xunit"
)

func Example() {
	ctx := context.Background()

	suite := xunit.TestSuite{
		Name:     "Example",
		Disabled: 17,
		Timestamp: xunit.NewTimestamp(
			time.Date(
				2022,
				time.December,
				24,
				15,
				0,
				0,
				0,
				errör.Must(time.LoadLocation("Europe/Stockholm")),
			),
		),
		Properties: []xunit.Property{
			{"foo", "bar"},
			{"bar", "baz"},
		},
	}

	suite.TestCases = append(suite.TestCases, xunit.TestCase{
		Name: "Case 1",
		Time: xunit.NewTime(42 * time.Microsecond),
	})

	suite.TestCases = append(suite.TestCases, xunit.TestCase{
		Name: "Case 2",
		Time: xunit.NewTime(10 * time.Second),
		Failure: &xunit.Failure{
			Message: "Something went wrong",
		},
	})

	suite.TestCases = append(suite.TestCases, xunit.TestCase{
		Name: "Case 2",
		Time: xunit.NewTime(time.Hour),
		Error: &xunit.Error{
			Message: "Error!",
			Type:    "internal",
		},
		SystemErr: []byte(`Panic!`),
	})

	suite.TestCases = append(suite.TestCases, xunit.TestCase{
		Name:    "Case 3",
		Skipped: &xunit.Skipped{Message: "N/A"},
	})

	suites := xunit.TestSuites{
		Name:       "Example",
		TestSuites: []xunit.TestSuite{suite},
	}

	suites.Print(
		ctx,
		os.Stdout,
		xunit.OptionAutoID(true),
		xunit.OptionTimeDecimals(6),
		xunit.OptionTimeGrouping(false),
	)

	// Output:
	// <?xml version="1.0" encoding="UTF-8"?>
	// <testsuites name="Example" tests="4" disabled="17" failures="1" errors="1">
	//   <testsuite id="0" name="Example" tests="4" disabled="17" failures="1" errors="1" skipped="1" timestamp="2022-12-24T14:00:00">
	//     <properties>
	//       <property name="foo" value="bar"></property>
	//       <property name="bar" value="baz"></property>
	//     </properties>
	//     <testcase id="0" name="Case 1" time="0.000042"></testcase>
	//     <testcase id="1" name="Case 2" time="10">
	//       <failure message="Something went wrong"></failure>
	//     </testcase>
	//     <testcase id="2" name="Case 2" time="3600">
	//       <error message="Error!" type="internal"></error>
	//       <system-err>Panic!</system-err>
	//     </testcase>
	//     <testcase id="3" name="Case 3">
	//       <skipped message="N/A"></skipped>
	//     </testcase>
	//   </testsuite>
	// </testsuites>
}

func TestParse(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	for _, c := range [...]struct {
		Name     string
		XML      string
		Expected *xunit.TestSuites
		Errors   []error
	}{
		{
			Name: "Sample test suites",
			XML: `<?xml version="1.0" encoding="UTF-8" ?>
   <testsuites id="20140612_170519" name="New_configuration (14/06/12 17:05:19)" tests="225" failures="1262" time="0.001">
      <testsuite id="codereview.cobol.analysisProvider" name="COBOL Code Review" tests="45" failures="17" time="0.001">
         <testcase id="codereview.cobol.rules.ProgramIdRule" name="Use a program name that matches the source file name" time="0.001">
            <failure message="PROGRAM.cbl:2 Use a program name that matches the source file name" type="WARNING">
WARNING: Use a program name that matches the source file name
Category: COBOL Code Review – Naming Conventions
File: /project/PROGRAM.cbl
Line: 2
      </failure>
    </testcase>
  </testsuite>
</testsuites>
`,
			Expected: &xunit.TestSuites{
				ID:   "20140612_170519",
				Name: "New_configuration (14/06/12 17:05:19)",
				Time: xunit.NewTime(time.Millisecond),
				TestSuites: []xunit.TestSuite{
					{
						ID:   "codereview.cobol.analysisProvider",
						Name: "COBOL Code Review",
						Time: xunit.NewTime(time.Millisecond),
						TestCases: []xunit.TestCase{
							{
								ID:   "codereview.cobol.rules.ProgramIdRule",
								Name: "Use a program name that matches the source file name",
								Time: xunit.NewTime(time.Millisecond),
								Failure: &xunit.Failure{
									Message: "PROGRAM.cbl:2 Use a program name that matches the source file name",
									Type:    "WARNING",
									Description: `WARNING: Use a program name that matches the source file name
Category: COBOL Code Review – Naming Conventions
File: /project/PROGRAM.cbl
Line: 2`,
								},
							},
						},
					},
				},
			},
		},

		{
			Name: "Sample, but only 1 test suite",
			XML: `<?xml version="1.0" encoding="UTF-8" ?>
    <testsuite id="codereview.cobol.analysisProvider" name="COBOL Code Review" tests="45" failures="17" time="0.001">
       <testcase id="codereview.cobol.rules.ProgramIdRule" name="Use a program name that matches the source file name" time="0.001">
          <failure message="PROGRAM.cbl:2 Use a program name that matches the source file name" type="WARNING">
WARNING: Use a program name that matches the source file name
Category: COBOL Code Review – Naming Conventions
File: /project/PROGRAM.cbl
Line: 2
    </failure>
  </testcase>
</testsuite>
`,
			Expected: &xunit.TestSuites{
				TestSuites: []xunit.TestSuite{
					{
						ID:   "codereview.cobol.analysisProvider",
						Name: "COBOL Code Review",
						Time: xunit.NewTime(time.Millisecond),
						TestCases: []xunit.TestCase{
							{
								ID:   "codereview.cobol.rules.ProgramIdRule",
								Name: "Use a program name that matches the source file name",
								Time: xunit.NewTime(time.Millisecond),
								Failure: &xunit.Failure{
									Message: "PROGRAM.cbl:2 Use a program name that matches the source file name",
									Type:    "WARNING",
									Description: `WARNING: Use a program name that matches the source file name
Category: COBOL Code Review – Naming Conventions
File: /project/PROGRAM.cbl
Line: 2`,
								},
							},
						},
					},
				},
			},
		},
	} {
		c := c

		t.Run(c.Name, func(t *testing.T) {
			t.Parallel()

			actual, err := xunit.Parse(ctx, strings.NewReader(c.XML))

			if len(c.Errors) > 0 {
				assert.Nil(t, actual)
				for _, e := range c.Errors {
					assert.ErrorIs(t, err, e)
				}
				return
			}

			require.NoError(t, err)
			assert.Equal(t, c.Expected, actual)
		})
	}
}
