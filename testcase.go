package xunit

import (
	"time"

	"gitlab.com/biffen/go-xunit/internal"
	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

var _ Statser = (*TestCase)(nil)

func countPointer[T any](t *T) uint64 {
	if t == nil {
		return 0
	}

	return 1
}

type Skipped struct {
	Message string
}

type TestCase struct {
	ID         string
	File       string
	Name       string
	Assertions uint64
	Classname  string
	Status     string
	Time       *time.Duration
	Skipped    *Skipped
	Error      *Error
	Failure    *Failure
	SystemOut  []byte
	SystemErr  []byte
}

func testCaseFromXML(x xunitxml.TestCase, options internal.Options) TestCase {
	c := TestCase{
		ID:         x.ID,
		File:       x.File,
		Name:       x.Name,
		Assertions: x.Assertions,
		Classname:  x.Classname,
		Status:     x.Status,
		Time:       x.Time.ToDuration(),
		Error:      errorFromXML(x.Error, options),
		Failure:    failureFromXML(x.Failure, options),
		SystemOut:  x.SystemOut,
		SystemErr:  x.SystemErr,
	}

	if x.Skipped != nil {
		c.Skipped = &Skipped{
			Message: x.Skipped.Message,
		}
	}

	return c
}

func (c *TestCase) Stats() (stats Stats) {
	return Stats{
		Tests:    1,
		Failures: countPointer(c.Failure),
		Errors:   countPointer(c.Error),
		Skipped:  countPointer(c.Skipped),
	}
}

func (c *TestCase) toXML(options internal.Options) xunitxml.TestCase {
	x := xunitxml.TestCase{
		ID:         c.ID,
		File:       c.File,
		Name:       c.Name,
		Assertions: c.Assertions,
		Classname:  c.Classname,
		Status:     c.Status,
		Time:       internal.NewTime(c.Time, options),
		Error:      c.Error.toXML(options),
		Failure:    c.Failure.toXML(options),
		SystemOut:  c.SystemOut,
		SystemErr:  c.SystemErr,
	}

	if c.Skipped != nil {
		x.Skipped = &xunitxml.Skipped{
			Message: c.Skipped.Message,
		}
	}

	return x
}
