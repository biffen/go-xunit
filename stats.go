package xunit

type Stats struct {
	Tests    uint64
	Disabled uint64
	Failures uint64
	Errors   uint64
	Skipped  uint64
}

func (s Stats) Merge(other Stats) Stats {
	s.Tests += other.Tests
	s.Disabled += other.Disabled
	s.Failures += other.Failures
	s.Errors += other.Errors
	s.Skipped += other.Skipped

	return s
}

type Statser interface {
	Stats() Stats
}
