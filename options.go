package xunit

import (
	"gitlab.com/biffen/go-xunit/internal"
)

var _ Option = (optionFunc)(nil)

func initOptions(options []Option) internal.Options {
	o := internal.NewOptions()
	for _, option := range options {
		option.apply(&o)
	}

	return o
}

type Option interface {
	apply(*internal.Options)
}

func OptionAutoID(autoID bool) Option {
	return optionFunc(func(options *internal.Options) {
		options.AutoID = autoID
	})
}

func OptionComment(comment string) Option {
	return optionFunc(func(options *internal.Options) {
		options.Comment = comment
	})
}

func OptionTimeDecimals(decimals uint8) Option {
	return optionFunc(func(options *internal.Options) {
		options.TimeDecimals = decimals
	})
}

func OptionTimeGrouping(grouping bool) Option {
	return optionFunc(func(options *internal.Options) {
		options.TimeGrouping = grouping
	})
}

type optionFunc func(*internal.Options)

func (f optionFunc) apply(options *internal.Options) {
	f(options)
}
