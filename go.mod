module gitlab.com/biffen/go-xunit

go 1.20

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/stretchr/testify v1.8.2
	gitlab.com/biffen/error v0.2.0
	gitlab.com/biffen/go-applause v0.4.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
