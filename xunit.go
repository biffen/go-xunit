package xunit

import (
	"bytes"
	"context"
	"encoding/xml"
	"errors"
	"io"
	"time"

	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

func Parse(
	ctx context.Context,
	r io.Reader,
	options ...Option,
) (*TestSuites, error) {
	o := initOptions(options)

	x, err := io.ReadAll(r)
	if err != nil {
		return nil, err
	}

	var tmp struct {
		XMLName xml.Name
	}

	if err := xml.NewDecoder(bytes.NewReader(x)).
		Decode(&tmp); err != nil {
		return nil, err
	}

	var (
		suites xunitxml.TestSuites
		target any
	)

	switch tmp.XMLName.Local {
	case "testsuites":
		target = &suites

	case "testsuite":
		suites.TestSuites = make([]xunitxml.TestSuite, 1)
		target = &suites.TestSuites[0]

	default:
		return nil, errors.New("FIXME")
	}

	if err := xml.NewDecoder(bytes.NewReader(x)).
		Decode(target); err != nil {
		return nil, err
	}

	return testSuitesFromXML(suites, o), nil
}

func NewTime(d time.Duration) *time.Duration {
	return &d
}

func NewTimestamp(t time.Time) *time.Time {
	return &t
}
