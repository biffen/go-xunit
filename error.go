package xunit

import (
	"fmt"
	"reflect"

	"gitlab.com/biffen/go-xunit/internal"
	xunitxml "gitlab.com/biffen/go-xunit/internal/xml"
)

type Error struct {
	Message     string
	Type        string
	Description string
}

func ErrorFromError(err error) *Error {
	if err == nil {
		return nil
	}

	t := reflect.TypeOf(err)
	for t.Kind() == reflect.Pointer {
		t = t.Elem()
	}

	return &Error{
		Message:     err.Error(),
		Type:        fmt.Sprintf("%s.%s", t.PkgPath(), t.Name()),
		Description: fmt.Sprintf("%+v", err),
	}
}

func errorFromXML(x *xunitxml.Error, options internal.Options) *Error {
	if x == nil {
		return nil
	}

	return &Error{
		Message:     x.Message,
		Type:        x.Type,
		Description: x.Description.String(),
	}
}

func (e *Error) toXML(options internal.Options) *xunitxml.Error {
	if e == nil {
		return nil
	}

	return &xunitxml.Error{
		Message:     e.Message,
		Type:        e.Type,
		Description: internal.NewText(e.Description),
	}
}
